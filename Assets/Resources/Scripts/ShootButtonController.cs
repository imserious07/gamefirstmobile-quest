﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootButtonController : MonoBehaviour {

    public GameObject player;
    public GameObject shot;
    private bool isShooting;
    public float interval;
    private float currentWait;

	// Use this for initialization
	void Start () {
        interval = 0.5f;
        currentWait = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (!GameController.gameOver) {
            if (currentWait > 0) {
                currentWait -= Time.deltaTime;
            }

            if (isShooting) {
                if (currentWait <= 0) {
                    currentWait = interval;
                    Shoot();
                }
            }
        }
	}

    public void Presed() {
        if (!GameController.gameOver) {
            isShooting = true;
        }
    }

    public void Relesed() {
        if (!GameController.gameOver) {
            isShooting = false;
        }
    }

    public void Shoot() {
        if (!GameController.gameOver) {
            GameObject shotGO = Instantiate(shot, player.transform.position,
                    Quaternion.AngleAxis(player.GetComponent<PlayerController>().playerAngle, Vector3.forward));
            shotGO.GetComponent<BulletController>().angle = player.GetComponent<PlayerController>().playerAngle;
            shotGO.GetComponent<BulletController>().Scale(GameController.b_size.x, GameController.b_size.y);
        }
    }
}
