﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tools : MonoBehaviour {

    // Радинаны в вектор
    public static Vector2 RadianToVector2(float radian) {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }

    // Градусы в вектор
    public static Vector2 DegreeToVector2(float degree) {
        return RadianToVector2(degree * Mathf.Deg2Rad);
    }

    // Векстор в радианы
    public static float Vector2ToRadian(Vector3 direction) {
        return Mathf.Atan2(direction.y, direction.x);
    }

    // Вектор в градусы
    public static float Vector2ToDegree(Vector3 direction) {
        float angleRads = Vector2ToRadian(direction);
        float angleDeg = angleRads * Mathf.Rad2Deg;

        if (angleDeg < 0)
            angleDeg += 360;

        return angleDeg;
    }

    // Get object in position
    // Получить объект в позиции
    public static GameObject getObjectAtPosition(Vector2 position) {
        Collider2D col = Physics2D.OverlapBox(position, Vector2.one * 0.1f, 0);
        if (col != null) {
            return col.gameObject;
        }
        return null;
    }
}
