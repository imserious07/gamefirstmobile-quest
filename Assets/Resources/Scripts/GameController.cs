﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    // Variables for spawn timer
    // Переменные для создания врагов
    public float intervalToSpawnEnemies = 0;
    private float readyToSpawnTime;

    // GUI objects
    // Объекты графического интерфейса

    private static GameObject GUIgameOver;

    // Debug variables
    // Переменные для отладки
    public bool drawBounds;
    public bool debagInfo;

    // Game control variables
    // Переменные для контроля над игрой
    public static bool gameOver = false;

    // Constants for enemy spawn
    // Константы для создания врагов
    private const int BOTTOM = 0;
    private const int TOP = 1;
    private const int LEFT = 2;
    private const int RIGHT = 3;
    private const float SPAWN_OFFSET = 0.5f;

    // Player health and score
    // Счёт и жизни игрока
    public static int Health;
    public static int Score;

    // For setting enemy prefub 
    // Для установки шаблона для врага
    public GameObject enemy;

    // World size 
    // Размер мира
    public static Vector2 w_size;

    // Cam size 
    // Размер камеры
    public static Vector2 c_size;

    // Map left top corner 
    // Левый верхний угол карты
    public static Vector2 l_t_corner;

    // Map right bottom corner 
    // Правый нижний угол карты
    public static Vector2 r_b_corner;

    // Player size 
    // Размер игрока
    public static Vector2 p_size;

    // Bullet size 
    // Размер снаряда
    public static Vector2 b_size;

    // Enemy size 
    // Размер врага
    public static Vector2 e_size;

    // Enemy vision range 
    // Область видимости врага
    public static float e_vision;

    // Player, bullet, enemy speeds 
    // Скорость игрока, снаряда, врагов
    public static float p_speed;
    public static float b_speed;
    public static float e_speed;

    public static float enemy_vision_range;

    // This method will work every frame 
    // Этот метод будет срабатывать каждый кадр 
    void Update() {
        // Spawn enemies 
        // Создание врагов
        spawnEnemies();
    }

    // Initialization 
    // Инициализация
    void Start () {
        // Game began
        // Игра началась
        gameOver = false;
        Score = 0;

        // Find and disable "Game over" object
        // Найти и отключить объект "Game over"
        GameObject[] gui = GameObject.FindGameObjectsWithTag("game control gui");

        foreach (GameObject go in gui) {
            if (go.name == "Game over") {
                GUIgameOver = go;
                go.SetActive(false);
                break;
            }
        }

        // Spawn enemy values 
        // Переменные отвечающие за создание врагов
        if (intervalToSpawnEnemies == 0) {
            intervalToSpawnEnemies = 0.5f;
        }

        readyToSpawnTime = intervalToSpawnEnemies;

        // Set static variables 
        // Задать все статичные переменные
        Health = 3;
        Score = 0;

        // Set all dynamic variables 
        // Задать все динамические переменные
        InitParams();

        GameObject player = GameObject.FindGameObjectWithTag("player");

        // Move player to center 
        // Двигать игрока в центр карты
        player.transform.position = new Vector3(GameController.l_t_corner.x + (GameController.w_size.x / 2),
                                         GameController.l_t_corner.y + (GameController.w_size.y / 2),
                                         player.transform.position.z);

        // Show Debug Info
        // Показать информацию для отладки
        if (debagInfo) {
            Debug.Log("Map size width: " + w_size.x);
            Debug.Log("Cam size width: " + c_size.x);
            Debug.Log("Player size on screen: " + p_size);
            Debug.Log("Player speed: " + p_speed);
            Debug.Log("World size: " + w_size);
        }

        // Drawing world bounds for debuging
        // Отрисовка границ мира для отладки
        if (drawBounds) {
            GameController.DrawRect(GameController.l_t_corner, GameController.r_b_corner);
        }

        // Scale player
        // Масштабирование игрока
        player.GetComponent<PlayerController>().Scale(p_size.x, p_size.y);
    }

    // Set all dynamic variables 
    // Задать все динамические переменные
    private void InitParams() {
        // Calculate camera size 
        // Расчёт размеров камеры
        float height = Camera.main.orthographicSize * 2;
        float width = height * Camera.main.aspect;
        c_size = new Vector2(width, height);
        float helf_c_size = c_size.x / 2;
        // Calculate world size 
        // Расчёт размеров мира
        w_size = new Vector2(c_size.x * 2, c_size.y * 2);
        // Calculate l_t_corner 
        // Расчёт левого верхнего угла
        l_t_corner = new Vector2(0, 0);
        // Calculate r_b_corner 
        // Расчёт правого нижнего угла
        r_b_corner = new Vector2(l_t_corner.x + w_size.x, l_t_corner.y + w_size.y);
        // Calculate player size 
        // Расчёт размеров игрока
        p_size = new Vector2(helf_c_size * 0.05f, helf_c_size * 0.05f);
        // Calculate enemy size 
        // Расчитать размер врага
        e_size = p_size;
        // Calculate bullet size 
        // Расчёт размеров снаряда
        b_size = new Vector2(helf_c_size * 0.05f, helf_c_size * 0.01f);
        // Calculate enemy vision range 
        // Расчёт области видимости врага
        e_vision = c_size.x * 0.2f;
        // Calculate player, enemy, bullet speeds 
        // Расчёт скоростей для игрока, врагов, снарядов
        p_speed = c_size.x * 0.1f;
        e_speed = p_speed;
        b_speed = c_size.x * 0.2f;
    }

    // Health down 
    // Снизить здоровье
    public static void healthDown() {
        if (Health == 0) {
            GameOver();
        } else {
            Health--;

            GameObject[] gos = GameObject.FindGameObjectsWithTag("hearth");
            foreach (GameObject hearth in gos) {
                if (hearth.name == "Hearth_" + Health) {
                    Destroy(hearth);
                    break;
                }
            }
        }
    }

    // Score up 
    // Увеличить очки
    public static void scoreUp() {
        Score++;

        GameObject.FindGameObjectWithTag("score").GetComponent<Text>().text = "score: " + Score;
    }

    // For Debaging (Line drawing)
    // Для тестирования (отрисовка линий)
    // ===============================================================================
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f) {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.SetColors(color, color);
        lr.SetWidth(0.1f, 0.1f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        // GameObject.Destroy(myLine, duration);
    }

    public static void DrawRect(Vector2 l_t_corner, Vector2 r_b_corner) {
        DrawLine(l_t_corner, new Vector2(r_b_corner.x, l_t_corner.y), Color.black);
        DrawLine(l_t_corner, new Vector2(l_t_corner.x, r_b_corner.y), Color.black);
        DrawLine(r_b_corner, new Vector2(r_b_corner.x, l_t_corner.y), Color.black);
        DrawLine(r_b_corner, new Vector2(l_t_corner.x, r_b_corner.y), Color.black);
    }
    // ===================================================================================

    // Create enemy
    // Создать врага
    private void spawnEnemies() {
        if (!gameOver) {
            readyToSpawnTime -= Time.deltaTime;

            if (readyToSpawnTime <= 0) {
                readyToSpawnTime = intervalToSpawnEnemies;

                int side = Random.Range(0, 4);

                // Get Pos To Spawn
                float[] info = setPosToSpawn(side);

                if (info[0] != 0 && info[1] != 0 && info[2] != 0) {
                    GameObject enemyGO = Instantiate(enemy, new Vector3(info[0], info[1], 0),
                                    Quaternion.AngleAxis(info[2], Vector3.forward));

                    enemyGO.GetComponent<EnemyController>().angle = info[2];

                    string GOname = "Enemy unknown";
                    switch (side) {
                        case TOP: GOname = "Enemy from top"; break;
                        case BOTTOM: GOname = "Enemy from bottom"; break;
                        case LEFT: GOname = "Enemy from left"; break;
                        case RIGHT: GOname = "Enemy from right"; break;
                    }
                    enemyGO.name = GOname;
                }
            }
        }
    }

    // Find position to spawn enemy
    // Найти позицию для создания врага
    private float[] setPosToSpawn(int side) {
        float spawnX = 0;
        float spawnY = 0;
        float enemyAngle = 0;
        float[] result = new float[3];

        switch (side) {
            case LEFT:
                spawnX = l_t_corner.x - e_size.x - SPAWN_OFFSET;
                spawnY = Random.Range(l_t_corner.y, r_b_corner.y);

                enemyAngle = Random.Range(-45, 45);

                break;
            case RIGHT:
                spawnX = r_b_corner.x + e_size.x + SPAWN_OFFSET;
                spawnY = Random.Range(l_t_corner.y, r_b_corner.y);

                enemyAngle = Random.Range(135, 225);

                break;
            case TOP:
                spawnX = Random.Range(l_t_corner.x, r_b_corner.x);
                spawnY = r_b_corner.y + e_size.y + SPAWN_OFFSET;

                enemyAngle = Random.Range(225, 315);

                break;
            case BOTTOM:
                spawnX = Random.Range(l_t_corner.x, r_b_corner.x);
                spawnY = l_t_corner.y - e_size.y - SPAWN_OFFSET;

                enemyAngle = Random.Range(45, 135);

                break;
        }

        result[0] = spawnX;
        result[1] = spawnY;
        result[2] = enemyAngle;

        // If some enemy one on current positon, select other
        // Если позиция уже занята другим объектом, выбрать другую
        if (Tools.getObjectAtPosition(new Vector2(spawnX, spawnY)) != null) {
            result = setPosToSpawn(side);
        }

        return result;
    }

    // Конец игры
    public static void GameOver() {
        if (GUIgameOver != null) {
            gameOver = true;
            GUIgameOver.SetActive(true);
        }
    }

}
