﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoyStick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {

    private Image bgImg;
    private Image joyImg;

    public Vector3 InputDirection;

    private void Start() {
        bgImg = GetComponent<Image>();
        joyImg = transform.GetChild(0).GetComponent<Image>();
        InputDirection = Vector2.zero;
    }

    public void OnDrag(PointerEventData ped) {
        if (!GameController.gameOver) {
            Vector2 pos = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
                bgImg.rectTransform,
                ped.position,
                ped.pressEventCamera,
                out pos)) {
                pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
                pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

                float x = (bgImg.rectTransform.pivot.x == 1f) ? pos.x * 2 + 1 : pos.x * 2 - 1;
                float y = (bgImg.rectTransform.pivot.y == 1f) ? pos.y * 2 + 1 : pos.y * 2 - 1;

                InputDirection = new Vector3(x, y, 0);

                InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;

                joyImg.rectTransform.anchoredPosition =
                    new Vector3(InputDirection.x * (bgImg.rectTransform.sizeDelta.x / 3),
                                InputDirection.y * (bgImg.rectTransform.sizeDelta.y / 3));

            }
        }
    }

    public void OnPointerDown(PointerEventData ped) {
        if (!GameController.gameOver) {
            OnDrag(ped);
        }
    }

    public void OnPointerUp(PointerEventData ped) {
        if (!GameController.gameOver) {
            InputDirection = Vector2.zero;
            joyImg.rectTransform.anchoredPosition = Vector3.zero;
        }
    }
}