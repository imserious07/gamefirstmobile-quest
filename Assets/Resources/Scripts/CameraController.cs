﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    GameObject player;
    public bool bounds;
    public bool folowPlayer;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("player");
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (!GameController.gameOver) {
            Borders();
        }
    }

    void Borders() {
        if (folowPlayer) {
            Vector3 newPosition = new Vector3(player.transform.position.x,
                                              player.transform.position.y,
                                              transform.position.z);

            if (bounds) {
                newPosition = new Vector3(
                    Mathf.Clamp(newPosition.x,
                                GameController.l_t_corner.x + GameController.c_size.x / 2,
                                GameController.r_b_corner.x - GameController.c_size.x / 2),
                    Mathf.Clamp(newPosition.y,
                                GameController.l_t_corner.y + GameController.c_size.y / 2,
                                GameController.r_b_corner.y - GameController.c_size.y / 2),
                    newPosition.z);
            }

            transform.position = newPosition;
        }
    }
}
