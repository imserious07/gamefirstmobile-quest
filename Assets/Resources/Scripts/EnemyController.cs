﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Entity {

    public GameObject player;

    private bool isEnterWorld = false;
    private bool isFrozen = false;
    private bool isDead = false;

    private GameObject target;

    float startScaleX;
    float startScaleY;
    float scaleDownX;
    float scaleDownY;

    float currentTimeToScale;

    // Use this for initialization
    void Start() {
        currentTimeToScale = 1;
        target = null;

        if (player == null) {
            player = GameObject.FindGameObjectWithTag("player");
        }
    }

    // Update is called once per frame
    void Update() {
        if (!GameController.gameOver) {
            // Kil when leave scene
            if (isObjectInWorld() && !isEnterWorld) {
                isEnterWorld = true;
            } else if (!isObjectInWorld() && isEnterWorld) {
                Destroy(gameObject);
            }

            // Check if target in vision range / Проверить находится ли цель в зоне видимости
            if (target == null) {
                float distanse = Vector2.Distance(transform.position, player.transform.position);

                if (distanse <= GameController.e_vision) {
                    target = player;
                }
            }

            // Move forward or on target / перемещать вперёд или к цели
            if (!isFrozen) {
                if (target == null) {
                    MoveForward(GameController.e_speed);
                } else {
                    MoveForward(GameController.e_speed, target.transform.position);
                }
            }

            // Kill enemy / Для убийства врага
            if (isDead) {
                Kill();
            }
        }
    }

    public void Kill() {
        if (!isDead) {
            isFrozen = true;
            startScaleX = gameObject.transform.localScale.x;
            startScaleY = gameObject.transform.localScale.y;
            isDead = true;
        }

        if (isDead) {
            currentTimeToScale -= Time.deltaTime;
            // Size down
            gameObject.transform.localScale = new Vector3(startScaleX * currentTimeToScale,
                                                            startScaleY * currentTimeToScale,
                                                            gameObject.transform.localScale.z);

            if (currentTimeToScale <= 0) {
                Destroy(gameObject);
            }
        }
    }

    // If enemy touch someone
    // Если враг касается кого-то
    void OnCollisionStay2D(Collision2D col) {
        if (!isDead) {
            GameObject otherGO = col.gameObject;

            if (otherGO.tag == "player") {
                PlayerController pc = otherGO.GetComponent<PlayerController>();

                if (!isFrozen && !pc.isInvulnerable) {
                    pc.GetHit();
                    StartCoroutine(WaitBeforeAwake(3));
                }
            }
        }
    }
    
    // If enemy start touching someone
    // Если враг начал косаться кого-то
    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "enemy") {
            Bounce();
        }
    }

        // If enemy enter in object
        // Если враг вошёл в объект
        void OnTriggerEnter2D(Collider2D other) {
        GameObject otherGO = other.gameObject;

        if (otherGO.tag == "bullet") {
            if (!isDead) {
                Kill();
                GameController.scoreUp();
                Destroy(otherGO);
            }
        }
    }

    // Wait befor awake
    // Ждать перед тем как сделать объект активным
    IEnumerator WaitBeforeAwake(float seconds) {
        isFrozen = true;
        yield return new WaitForSeconds(seconds);
        isFrozen = false;
    }
}
