﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : Entity {
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!GameController.gameOver) {
            MoveForward(GameController.b_speed);

            // Destroy when leave world
            if (!isObjectInWorld()) {
                Destroy(gameObject, 0.5f);
            }
        }
    }

    
}
