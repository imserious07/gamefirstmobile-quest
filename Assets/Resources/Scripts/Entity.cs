﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {

    public float angle;

    public void Scale(float sizeW, float sizeH) {
        Sprite s = GetComponent<SpriteRenderer>().sprite;

        float pixelsPerUnit = s.pixelsPerUnit;
        float imgWidth = s.rect.width;
        float imgHeight = s.rect.height;

        float WidthInUnits = pixelsPerUnit / 100;

        transform.localScale = new Vector3((GameController.c_size.x / 8) * sizeW,
                                      (GameController.c_size.x / 8) * sizeH,
                                      transform.localScale.z);
    }

    public bool isObjectInWorld() {
        Rect world = new Rect(GameController.l_t_corner.x, 
                              GameController.l_t_corner.y, 
                              GameController.w_size.x, 
                              GameController.w_size.y);

            return world.Contains(transform.position);
    }

    // Move bullet
    public void MoveForward(float speed) {
        float step = speed * Time.deltaTime;

        Vector3 pointToMove = new Vector3(step * Mathf.Cos(angle * Mathf.Deg2Rad),
                                          step * Mathf.Sin(angle * Mathf.Deg2Rad),
                                          transform.position.z);

        transform.position = Vector3.MoveTowards(transform.position,
                                                      transform.position + pointToMove,
                                                      step);
    }

    public void MoveForward(float speed, Vector3 pointToMove) {
        float step = speed * Time.deltaTime;

        transform.position = Vector3.MoveTowards(transform.position,
                                                      pointToMove,
                                                      step);
    }

    public void Bounce() {
        angle -= 180;

        if (angle < 0)
            angle += 360;
    }
}
