﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Entity {

    public VirtualJoyStick vj;

    // Invulnerable
    public bool isInvulnerable;
    public float intervalOfBlinking;
    public float blinkTimer;

    private Vector3 startPos;
    private Vector3 target;
    private float timeToReachTarget;
    private float t;

    public float playerSize;
    public float playerAngle;

    // Use this for initialization
    void Start() {
        startPos = target = transform.position;
        isInvulnerable = false;

        intervalOfBlinking = 0.2f;
        blinkTimer = intervalOfBlinking;

        // Get player width
        playerSize = gameObject.GetComponent<Renderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update() {
        if (!GameController.gameOver) {
            // Rotate player
            setPlayerRotation(vj.InputDirection);
            // Move player
            moveGameObject(vj.InputDirection);
            // Player blink when Invulnerable
            PlayerBlinking();
        }
    }

    private float setPlayerRotation(Vector3 direction) {
        if (direction != null && !direction.Equals(Vector3.zero)) {
            playerAngle = Tools.Vector2ToDegree(direction);

            transform.rotation = Quaternion.AngleAxis(playerAngle, Vector3.forward);
            return playerAngle;
        }

        return 0;
    }

    // Player touch enemy
    // Игрок дотронулся до врага
    public void GetHit() {
        StartCoroutine(WaitBeforeVulnerability(2));
        GameController.healthDown();
    }

    private void moveGameObject(Vector3 target) {
        if (target != null && !target.Equals(Vector3.zero)) {
            float step = GameController.p_speed * Time.deltaTime;

            Vector3 newPosition = Vector3.MoveTowards(transform.position,
                                                      transform.position + target,
                                                      step);

            // Map borders
            float halfPlayerSize = playerSize / 2;

            if (newPosition.x < 0 + halfPlayerSize)
                newPosition.x = 0 + halfPlayerSize;
            if (newPosition.x > GameController.w_size.x - halfPlayerSize)
                newPosition.x = GameController.w_size.x - halfPlayerSize;
            if (newPosition.y < 0 + halfPlayerSize)
                newPosition.y = 0 + halfPlayerSize;
            if (newPosition.y > GameController.w_size.y - halfPlayerSize)
                newPosition.y = GameController.w_size.y - halfPlayerSize;

            transform.position = newPosition;
        }
    }

    // Start timer, then turn of invulnerable
    // Запускает таймер, по окончанию таймера выключает неуязвимость
    IEnumerator WaitBeforeVulnerability(float seconds) {
        Debug.Log("isInvulnerable: " + isInvulnerable);

        isInvulnerable = true;
        yield return new WaitForSeconds(seconds);
        isInvulnerable = false;

        Debug.Log("isInvulnerable: " + isInvulnerable);

        GetComponent<SpriteRenderer>().enabled = true;
    }

    // Player blinking
    // Мерцание игрока
    private void PlayerBlinking(){
        if (isInvulnerable) {
            blinkTimer -= Time.deltaTime;

            if (blinkTimer <= 0) {
                blinkTimer = intervalOfBlinking;

                SpriteRenderer spr = GetComponent<SpriteRenderer>();
                spr.enabled = !spr.enabled;
            }
        }
    }
}
